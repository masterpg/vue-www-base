import {AppStore} from '@/store'

declare interface TestModule<S> {
  $appStore: AppStore
}
