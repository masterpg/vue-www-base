module.exports = {
  plugins: {
    'autoprefixer': {},
    'postcss-import': {},
    'postcss-nesting': {},
    'postcss-extend': {},
    'postcss-mixins': {},
    'postcss-each': {},
    'postcss-for': {},
  },
}
